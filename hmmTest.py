import collections
import hmm
import json
import naiveBayes
import pickle
import random
import utils
import YelpReview

# SAVE THE HMM MODEL AT THIS PATH
HMM_MODEL_FILE = "hmm_model"
# SAVE THE NAIVE BAYES MODEL AT THIS PATH
NB_MODEL_FILE = "nb_model"

"""
Function: saveHMM
Generate an HMM and save the object.
@param labeledReviewsJSON - JSON document of labeled reviews.
@param naiveBayesModel - Naive Bayes model.
@param numStates - number of states to use in the HMM.
@param numFeatures - number of features used to define each sentence.
"""
def saveHMM(labeledReviewsJSON, naiveBayesModel, numStates, numFeatures):
    model = hmm.trainHMM(labeledReviewsJSON, naiveBayesModel, numStates, numFeatures)
    with open(HMM_MODEL_FILE, 'wb') as output: pickle.dump(model, output, pickle.HIGHEST_PROTOCOL)

"""
Function: loadHMM
Open saved HMM file and return as an HMM object. Hardcoded to filename HMM_MODEL_FILE.
"""
def loadHMM():
    with open(HMM_MODEL_FILE, 'rb') as input: hmm = pickle.load(input)
    return hmm

"""
Function: createHVJSON
Create either trainSet.json or testSet.json, which is a subset of labeled reviews that will be used for
creating the training set or test set.
@param indices - list of indices used to identify review IDs.
@param reviewItems - list of tuples (review ID, list of sentence indices) for labeled reviews.
@param flag - "train" or "test" determines which output file is used.
"""
def createHVJSON(indices, reviewItems, flag):
    outputDict = {}
    for i in indices:
        key, value = reviewItems[i]
        outputDict[key] = value
    
    if flag == "train": output = open("trainSet.json", 'w')
    elif flag == "test": output = open("testSet.json", 'w')

    json.dump(outputDict, output, indent=2)
    output.close()

"""
Function: loadTrainingData
Very compute intensive task. Don't want to do this too often.
@param trainSet - JSON file containing the training data.
@return - list of review objects.
"""
def loadTrainingData(trainSet):
    reviews = utils.loadLabeledReviews(trainSet)
    reviewObjects = []
    for reviewID in reviews.keys():
        review = utils.getReview(reviewID)
        print "(Loading Training Data) Retrieving Review: %s" % reviewID
        reviewObjects.append(review)

    return reviewObjects


"""
Function: holdoutValidation
Divide the labeled reviews into ten subsets, which each subset consisting of 1/10 of the review IDs found in
the labeled reviews chosen at random. One out of ten of the subsets will be used as the test set, while the
remaining subsets will be used as the training set. This is repeated ten times until each subset has been
used as a test set once. The F1 score is calculated for each unique pair of test/training set, and saved in
the file "F1scores.txt".
"""
def holdoutValidation():
    allReviews = utils.loadLabeledReviews("Labeled_Reviews_All.json")
    allReviews_items = allReviews.items()
    allReviews_length = len(allReviews)

    reviewIndices = random.sample(range(allReviews_length), allReviews_length)
    reviewIndices_length = len(reviewIndices)
    numGroups = 10
    div = reviewIndices_length/numGroups
    count = 0
    
    F1output = open("F1scores.txt", 'w')
    for i in range(numGroups):
        test = reviewIndices[count:count+div-1]
        train = reviewIndices[0:count] + reviewIndices[count+div:reviewIndices_length]
        createHVJSON(test, allReviews_items, "test")
        createHVJSON(train, allReviews_items, "train")
        # Load training data to generate summary sentence corpus and Naive Bayes model.
        trainSet = loadTrainingData("trainSet.json")
        
        score = generateF1Score(trainSet)
        F1output.write(str(i) + ": " + str(score) + "\n")
        count += div
    F1output.close()

"""
Function: generateF1Score
Calculates F1 score for every review in testSet.json using trainSet.json as training data. A Naive Bayes 
model and HMM is created from the training data. For each review, the user-chosen summary sentences is then
compared with the HMM generated summary sentences. The HMM will generate a summary length equivalent to the
human summary length. Ttrue positives, false positives, and false negatives are counted for every review
within the test set, and then used to calculate the F1 score for the entire test set.
@param trainSet - list of review objects composing training data.
@return - F1 score generated from pair of training data and test set.
"""
def generateF1Score(trainSet):
    # create new summary sentence corpus as training set changes
    utils.setSummarySentenceCorpus("trainSet.json", trainSet)
    
    # create new Naive Bayes model and HMM as training set changes
    naiveBayes.saveNB("trainSet.json", trainSet, NB_MODEL_FILE)
    naiveBayesModel = naiveBayes.loadNB(NB_MODEL_FILE)
    saveHMM("trainSet.json", naiveBayesModel, 3, 5)
    model = loadHMM()
    print model.M
    print model.p
    model.B.printResults()

    test = utils.loadLabeledReviews("testSet.json")
    count = 0
    TP = 0.0
    FP = 0.0
    FN = 0.0
    
    for key, value in test.items():
        r = utils.getReview(key, naiveBayesModel)
        try:
            predicted = model.summarize(r, len(value))
            print "Review ID", key
            print "HMM predicted indices: ", predicted
            print "Human selected indices: ", value
            tp = 0.0
            fp = 0.0
            fn = 0.0
            for i in predicted:
                if i in value: tp += 1
                else: fp += 1
            for i in value:
                if i not in predicted: fn += 1
            TP += tp
            FP += fp
            FN += fn

        except ZeroDivisionError:
            print "ZeroDivisionError Encountered with review ", key, "\n"
            count += 1
            continue

    P = TP/(TP + FP)
    R = TP/(TP + FN)
    F1 = 2 * P * R / (P + R)
    print "Total F1", F1
    return F1

"""
Function: showAverageF1
Calculate average F1 score across all test sets and display.
"""
def showAverageF1():
    score = 0
    handle = open("F1scores.txt", 'r')
    for line in handle: score += float(line.split()[1])
    handle.close()
    print "Average F1: ", score/10

if __name__ == '__main__':
    holdoutValidation()
    showAverageF1()



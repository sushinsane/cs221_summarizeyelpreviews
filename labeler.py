"""
#########################################################################
Created by: Sean Shi
Created date: 11/12/14
Modified data: 12/15/14
-------------------------------------------------------------------------
Script that allows user to choose summary sentences from a Yelp review
and save the result in a dictionary with keys as reviewID and values
that are a list of sentence positions within the document that have been
chosen to represent the review.
-------------------------------------------------------------------------
#########################################################################
"""

import json
import sys
import utils
import YelpReview

# REPLACE WITH YOUR OUTPUT FILE PATH HERE
LABELED_REVIEWS = "labeled_reviews.json"

# MINIMUM CHARACTER LIMIT ON REVIEW LENGTH
CHARACTER_MINIMUM = 500

def printUsageAndExit():
    print "USAGE: python labeler.py [start_index] [end_index]"
    print "[start_index]: the last review that was previously labeled."
    print "[end_index]: the review that you plan to review up to."
    sys.exit(0)

if __name__ == '__main__':
    
    # Ensure that user provides argument that specifies the range of reviews to be labeled.
    if len(sys.argv) != 3:
        print "Incorrect number of arguments"
        printUsageAndExit()
    
    # Ensure valid integer range is provided.
    try:
        startInd = int(sys.argv[1])
        endInd = int(sys.argv[2])
    except ValueError:
        print "Please insert valid range of integers"
        printUsageAndExit()

    # Pulls in all business IDs that are classified as restuarants.
    restaurantIDs = set(utils.getRestaurantIDs())

    handle = open(utils.reviewJSON, 'r')

    # initialize dictionary and review index
    reviewDict = {}
    count = 0

    # Display the restaurant reviews within the provided range.
    for line in handle:
        if count > endInd: break
        review = json.loads(line)
        if review["business_id"] in restaurantIDs:
            if len(review["text"]) >= CHARACTER_MINIMUM:
                if count < startInd:
                    count += 1
                    continue
                id = review["review_id"]
                
                # Output review contents
                print "[Review %d]:" % (count+1)
                print "Stars Given: %d" % review["stars"]
                print review["text"] + "\n"
                
                # Cycle through each sentence and wait for user input.
                # Only user input 'y' will register the sentence as a summary sentence.
                reviewText = utils.processReviewText(review["text"])
                indices = []
                for i in range(len(reviewText)):
                    print reviewText[i]
                    user_input = raw_input("Summary sentence (y/n)?: ")
                    if user_input == "y": indices.append(i)
                
                # Check that at least one summary sentence is chosen.
                # Open, write/overwrite, close to save intermediate progress.
                if indices:
                    reviewDict[id] = indices
                    output = open(LABELED_REVIEWS, 'w')
                    json.dump(reviewDict, output, indent=2)
                    output.close()
                else:
                    print "No summary sentences chosen. Omitting from output file...\n"

                count += 1

    handle.close()
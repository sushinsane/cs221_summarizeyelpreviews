"""
#########################################################################
Created by: Sean Shi
Created date: 11/13/14
Modified data: 12/15/14
-------------------------------------------------------------------------
Helpful functions for manipulating JSON documents into from consumable by
HMM and YelpReview.
-------------------------------------------------------------------------
#########################################################################
"""

import collections
import json
import re
import string
import YelpReview

# REPLACE WITH YOUR FILE-PATH TO yelp_academic_dataset_business.json HERE
businessJSON = r"../yelp_academic_dataset_business.json"
# REPLACE WITH YOUR FILE-PATH TO yelp_academic_dataset_review.json HERE
reviewJSON = r"../yelp_academic_dataset_review.json"

# Read in file of stop words. Stop words are common terms that hold little value
# in the context of understanding the document when assessed alone. Many functions
# utilize this list so load it only once for efficiency.
handle = open("stopWords.txt", 'r')
STOPWORDS = set([line.strip() for line in handle.readlines()])
handle.close()

# Container of all punctuation to be removed from strings
REGEX = re.compile('[%s]' % re.escape(string.punctuation))

# Intialize baseline corpus and summary sentence corpus to empty dictionaries.
BASELINE = {}
SS = {}

"""
Function: getRestuarantIDs
Pull out the first [numIDs] number of business IDs that are categorized
as a Restaurant. There are a total of 14303 business that are categorized
as Restaurant. Since it takes < 2 seconds to pull out all business IDs, all
restaurant IDs are pulled out by default if no argument is provided.
@param numIDs - number of business IDs to extract
@return - list of business IDs
"""
def getRestaurantIDs(numIDs=14303):
    handle = open(businessJSON, 'r');
    count = 0
    restaurantIDs = []
    for line in handle:
        # Break from document parsing if input limit has reached
        if count >= numIDs: break
        business = json.loads(line)
        if "Restaurants" in business["categories"]:
            restaurantIDs.append(business["business_id"])
            count += 1
    handle.close()
    return restaurantIDs

"""
Function: loadLabeledReviews
@param jsonFilename - filename of the JSON document containing user labeled reviews
@return - dictionary with ReviewIDs as keys and corresponding lists of sentence 
indices as values.
"""
def loadLabeledReviews(jsonFilename):
    handle = open(jsonFilename)
    reviewDict = json.loads(handle.read())
    handle.close()
    return reviewDict

"""
Function: processReviewText
@param text - text of a Yelp review.
@return - list of sentences in the review. sentences are split on punctuation (.?!).
newline characters are removed from sentences.
"""
def processReviewText(text):
    return [sentence.strip() \
            for sentence in re.split("[.?!]", text) \
            if sentence.strip() != ""]

"""
Function: processSentenceText
Take in a sentence string and process individual words as tokens. Replace all
punctuation from token with spaces, and convert tokens to lowercase. Append to
return list if the token does not belong to the set of STOPWORDS.
@param text - text of a sentence belonging to a Yelp Review.
@ return - list of terms found in sentence.
"""
def processSentenceText(text):
    tokens = []
    for token in REGEX.sub(' ', text).lower().split():
        if token not in STOPWORDS: tokens.append(token)
    return tokens

"""
Function: getReview
Look through all reviews in Yelp dataset until reviewID matches input ID. 
Construct review object from matching review.
@param id - unique review ID.
@param naiveBayesModel - Naive Bayes Model used to determine sentence feature.
@return - Yelp Review object.
"""
def getReview(id, naiveBayesModel=0):
    handle = open(reviewJSON, 'r')
    for line in handle:
        review = json.loads(line)
        if review["review_id"] == id:
            # Review located
            reviewText = processReviewText(review["text"])
            r = YelpReview.Review(id)
            r.ProcessTerms(reviewText)
            r.ProcessSentences(reviewText, naiveBayesModel)
            break

    handle.close()
    return r

"""
Function: createRestaurantCorpus
There are 1,125,458 total reviews. Of those 706,646 reviews are listed under the
category Restaurants. This function builds a corpus.json file that is a counter
of all the terms in the 706,646 reviews excluding punctuation and stop words. This
file serves as the baseline used to calculate baseline term probability as a feature.
"""
def createRestaurantCorpus():
    # convert to set for faster membership testing
    restaurantIDs = set(getRestaurantIDs())
    corpus = collections.Counter()
    handle = open(reviewJSON, 'r')

    count = 0
    for line in handle:
        review = json.loads(line)
        if review["business_id"] in restaurantIDs:
            count += 1
            print count
            for token in REGEX.sub(' ', review["text"]).lower().split():
                if token not in STOPWORDS: corpus[token] += 1

    handle.close()

    handle = open("corpus.json", 'w')
    json.dump(corpus, handle, indent=2)
    handle.close()

"""
Function: createSummarySentenceCorpus
For each summary sentence in the dataset of labeled reviews, build a corpus of terms.
This file serves as the baseline used to calculate summary sentence probability as a 
feature.
@param labeledReviews - JSON file containing user-labeled reviews for training set.
"""
def createSummarySentenceCorpus(labeledReviews):
    corpus = collections.Counter()
    reviews = loadLabeledReviews(labeledReviews)
    for key, value in reviews.items():
        print "Processing ", key, " to generate summary sentence corpus"
        review = getReview(key)
        for ind in value:
            s = review.sentences[ind]
            for term in processSentenceText(s.text):
                corpus[term] += 1

    handle = open("corpus_ss.json", 'w')
    json.dump(corpus, handle, indent=2)
    handle.close()

"""
Function: setSummarySentenceCorpus
Upon using a new training set, reset the value of the summary sentence corpus.
@param trainSetJSON - JSON file containing user-labeled reviews for training set.
@param trainSet - list of review objects composing training data.
"""
def setSummarySentenceCorpus(trainSetJSON, trainSet):
    corpus = collections.Counter()
    reviews = loadLabeledReviews(trainSetJSON)
    for review in trainSet:
        indices = reviews[review.id]
        for ind in indices:
            s = review.sentences[ind]
            for term in processSentenceText(s.text):
                corpus[term] += 1

    handle = open("corpus_ss.json", 'w')
    json.dump(corpus, handle, indent=2)
    handle.close()
    SS = loadLabeledReviews("corpus_ss.json")

"""
Function: filterReviews
Some labeled reviews might have no chosen summary sentences. This function serves to
remove these reviews from the document. Overwrite existing document of labeled reviews.
@param reviewJSON - JSON file containing user-labeled reviews.
"""
def filterReviews(reviewJSON):
    reviews = utils.loadLabeledReviews(reviewJSON)
    outputDict = {}
    for key, value in reviews.items():
        if key not in outputDict:
            try:
                len(value)
                outputDict[key] = value
            except TypeError:
                print key, value
                continue

    output = open(reviewJSON, 'w')
    json.dump(outputDict, output, indent=2)
    output.close()

"""
Function: getOccurancesFromCorpus
@param termCounter - dictionary of terms.
@return - aggregate occurance of input terms found within baseline.
"""
def getOccurancesFromCorpus(termCounter):
    total = 0
    for term in termCounter: total += BASELINE[term]
    return total

"""
Function: getOccurancesFromCorpus_SS
@param termCounter - dictionary of terms.
@return - aggregate occurance of input terms found within baseline.
"""
def getOccurancesFromCorpus_SS(termCounter):
    total = 0
    for term in termCounter:
        try:
            freq = SS[term]
        except KeyError: freq = 0
        total += freq
    return total

"""
Function: getFreqFromCorpus
@param term - term.
@return - frequency of occurrence of term within baseline of reviews.
"""
def getFreqFromCorpus(term):
    try: return BASELINE[term]
    except KeyError: return 0

"""
Function: getFreqFromCorpus_SS
@param term - term.
@return - frequency of occurrence of term within baseline of summary sentences.
"""
def getFreqFromCorpus_SS(term):
    try: return SS[term]
    except KeyError: return 0

# Preload baseline corpus to avoid reloading for each review.
# If file does not exist, create it.
try:
    BASELINE = loadLabeledReviews("corpus.json")
except IOError:
    print "corpus.json not found...attempting to create it."
    createRestaurantCorpus()
    BASELINE = loadLabeledReviews("corpus.json")
    print "baseline corpus generated successfully!"
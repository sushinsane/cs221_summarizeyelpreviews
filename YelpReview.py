"""
#########################################################################
File: YelpReview.py
Created by: Sean Shi
Created date: 11/12/14
Modified date: 12/14/14
#########################################################################
"""
import collections
import math
import naiveBayes
import utils

"""
Class: Sentence
Representation of a sentence within a review as a feature vector. Each sentence is composed of
the following five features...
numTerms:       number of terms found within the sentence.
baselineProb:   likelihood that terms within the sentence appear given a baseline of reviews.
documentProb:   likelihood that terms within the sentence appear given the review document.
ssProb:         likelihood that terms within the sentence appear given labeled summary sentences.
nbResult:       result of the Naive Bayes classification
"""
class Sentence(object):
    
    # Init
    # ----
    def __init__(self, index, numTerms, baselineProb, documentProb, ssProb, nbResult, text):
        # feature vector
        self.phi = [numTerms + 1, baselineProb, documentProb, ssProb, nbResult]
        # original sentence text
        self.text = text


"""
Class: Review
Parses text of a Yelp review into the following constituents:
ID:                 Unique identifier for the review.
Sentences:          List of sentence objects that comprise the review.
Terms:              Dictionary of tokens in the review that are not stop words.
                    Keys are the tokens, values are the frequencies of the tokens.
Document Frequency: Summation of all term frequencies of terms found within the review.
Baseline Frequency: Summation of all term frequencies of terms found within baseline of reviews.
SS Frequency:       Summation of all term frequences of terms found within labeled summary sentences.
"""
class Review(object):
    
    # Init
    # ----
    def __init__(self, reviewID):
        self.id = reviewID
        self.sentences = []
        self.terms = collections.Counter()
        self.documentFreq = 0
        self.baselineFreq = 0
        self.ssFreq = 0
    
    # Function: ProcessTerms
    # ----------------------
    # Builds a dictionary of tokens. Keys are the token strings.
    # Values are the corresponding frequencies of the token strings.
    # @param reviewText - a list of sentences split on .?! punctuation.
    def ProcessTerms(self, reviewText):
        for sentence in reviewText:
            # Split sentence on whitespace to parse individual tokens.
            # Only store lowercase version of tokens without any punctuation.
            tokens = utils.processSentenceText( sentence )
            for token in tokens: self.terms[token] += 1
        self.documentFreq = float(len(list( self.terms.elements() )))
        self.baselineFreq = float(utils.getOccurancesFromCorpus(self.terms))
        self.ssFreq = float(utils.getOccurancesFromCorpus_SS(self.terms)) + len(self.terms)

    # Function: ProcessSentences
    # --------------------------
    # Builds a list of Sentence Objects. Each sentence object is a feature vector.
    # @param reviewText - a list of sentences split on .?! punctuation.
    def ProcessSentences(self, reviewText, naiveBayesModel=0):
        for i in range( len(reviewText) ):
            # determine Naive Bayes classification of the sentence
            if not naiveBayesModel: nbResult = float('nan')
            else: nbResult = naiveBayes.classify(naiveBayesModel, reviewText[i])
            
            # determine term probabilities
            terms = utils.processSentenceText(reviewText[i])
            baselineProb = 0
            documentProb = 0
            ssProb = 0
            for term in terms:
                baselineTermFreq = utils.getFreqFromCorpus(term)
                baselineProb += math.log(baselineTermFreq/self.baselineFreq)
                docTermFreq = self.terms[term]
                documentProb += math.log(docTermFreq/self.documentFreq)
                ssTermFreq = utils.getFreqFromCorpus_SS(term) + 1
                ssProb += math.log(ssTermFreq/self.ssFreq)

            s = Sentence(i, len(terms), baselineProb, documentProb,
                         ssProb, nbResult,  reviewText[i])
            self.sentences.append(s)

    # Function: PrintContents
    # -----------------------
    # Prints out contents of the Review object.
    def PrintContents(self):
        print "Review ID:", self.id
        print "Term frequencies in document", self.documentFreq
        print "Term frequencies in baseline", self.baselineFreq
        print "Term frequencies in labeled summary sentences", self.ssFreq
        print "Terms:"
        for k, v in self.terms.items(): print k, v
        print "Sentences"
        for sentence in self.sentences:
            print "Number of Terms", sentence.phi[0]
            print "Baseline Probability", sentence.phi[1]
            print "Document Probability", sentence.phi[2]
            print "Summary Sentence Probability", sentence.phi[3]
            print "Naive Bayes Result", sentence.phi[4]
            print sentence.text










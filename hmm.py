"""
#########################################################################
Created by: Nitin Kapania
Edited by: Sean Shi
Created date: 11/26/14
Edit date: 12/16/14
-------------------------------------------------------------------------
Library of methods for HMM, specific to Yelp sentence extraction.
Requires numpy package to be installed. 
-------------------------------------------------------------------------
#########################################################################
"""

import utils
import YelpReview
import numpy as np
from scipy.stats import chi2

"""
class: HiddenMarkovModel
Representation of the hidden markov model used to determine Yelp summary sentences.
M:              Transition probability matrix. M is a (2N + 1) x (2N + 1) Markov transition matrix.
B:              Collection of multivariable functions.
p:              Probability distribution of initial sentence. p is a (2N + 1) distribution.
numStates:      Number of summary states to be used in the HMM.
numFeatures:    Number of features used to represent each sentence. Refer to YelpReview.py for more detail.
"""
class HiddenMarkovModel(object):
    def __init__(self, numStates, numFeatures):
        self.M = np.zeros((numStates, numStates))
        self.B = MultivariableNormals(numStates, numFeatures)
        self.p=  np.zeros((1, numStates))
        self.numStates = numStates
        self.numFeatures = numFeatures

    def incrementP(self, state):
        self.p[0][state] += 1

    def incrementM(self, state, newState):
        self.M[state][newState] += 1

    def normalize(self):
        self.B.normalizeSigma()
        norm(self.M)
        norm(self.p)

    # Summarize a YelpReview object in k sentences
    def summarize(self, review, k, verbose=0):
        D = FeatureSimilarityMatrices(self, review)
        T = len(review.sentences)
        w = sum( self.getAlpha(T-1, D) )

        gamma = []
        for t in range(T):
            score = 0.0
            alpha = self.getAlpha(t, D)
            beta =  self.getBeta(t, D)
            for state in range(self.numStates):
                # odd -> summary state of HMM
                if (state % 2):
                    score -= float(alpha[state])*float(beta[state])/float(w)
            gamma.append(score)

        bestIndices = np.array(gamma).argsort()

        indices = sorted(bestIndices[:k])
        for ind in indices: print review.sentences[ind].text
        return indices

    # Recursively compute alpha score.
    def getAlpha(self, t, D):
        if t == 0: return np.asmatrix(self.p).T
        else: return D.get(t) * np.asmatrix(self.M).T * self.getAlpha(t-1, D)

    # Recursively compute beta score.
    def getBeta(self, t, D):
        if t == D.len()-1: return np.ones( (self.numStates, 1) )
        else: return np.asmatrix(self.M) * D.get(t+1) * self.getBeta(t+1, D)

"""
class: MultivariableNormals
Collection of multivariable functions used to determine transition and emission probabilities.
mu:             Feature means.
sigma:
counts:
numStates:      Number of summary states to be used in the HMM.
numFeatures:    Number of features used to represent each sentence. Refer to YelpReview.py for more detail.
"""
class MultivariableNormals(object):
    def __init__(self, numStates, numFeatures):
        self.mu = []
        self.sigma = np.asmatrix(np.zeros( (numFeatures, 1) ) )
        self.counts = []
        self.numStates = numStates
        self.numFeatures = numFeatures
        for i in range(numStates):
            self.counts.append(0)
            self.mu.append(np.asmatrix(np.zeros( (numFeatures, 1) ) ) )

    def updateMu(self, state, phi):
        phiM = np.asmatrix(phi).T
        self.mu[state] = (self.mu[state]*self.counts[state] + phiM)/(self.counts[state] + 1)
        self.counts[state] += 1

    def updateSigma(self, state, phi):
        temp = np.asmatrix(phi).T - self.mu[state]
        cov = temp * temp.T
        self.sigma = self.sigma + cov

    def normalizeSigma(self):
        self.sigma = self.sigma / sum(self.counts)

    def printResults(self):
        print "Sigma: "
        print self.sigma
        for state in range( self.numStates ):
            print "State %d : Counts %d" % (state, self.counts[state])
            print "Feature Mu: "
            print self.mu[state]
            print ""

"""
class: FeatureSimilarityMatrices
Description
D:
Sinv:
"""
class FeatureSimilarityMatrices(object):
    def __init__(self, hmm, review):
        self.D = []
        Sinv = np.linalg.inv(hmm.B.sigma)
        
        for sentence in review.sentences:
            D_o = np.asmatrix(np.zeros( (hmm.numStates, hmm.numStates) ) )
            dof = hmm.numFeatures
            for state in range(hmm.numStates):
                phiM = np.asmatrix(sentence.phi).T
                arg = phiM - hmm.B.mu[state]
                D_o[state,state] = 1 - chi2.cdf(arg.T*Sinv*arg, dof)

            self.D.append(D_o)

    def len(self): return len(self.D)

    def get(self, sentence): return self.D[sentence]

    def printResults(self):
        print "Object Contains %d Matrices" % len(self.D)
        for i in range(len(self.D)):
            print self.D[i]
            print ""
                            

"""
Function: trainHMM
Construct HMM model from training set. Determine state transition matrix and p distribution of initial
sentences from labeled data.
@param labeledReviews - JSON document of labeled reviews to be used as training set.
@param naiveBayesModel - Naive Bayes model generated from the training data.
@param N - number of summary states to be used in HMM.
@param numFeatures - number of features associated with each sentence.
"""
def trainHMM(labeledReviews, naiveBayesModel, N=6, numFeatures=5):
    # Function to query whether a sentence is a summary sentence or not.
    def getSummaryFunc(summaryList): return lambda x: x in summaryList

    # Determine number of states from input N. N=1 is special case - simple two state model.
    numStates = 2 if N == 1 else 2*N+1

    # Initialize hidden markov model.
    hmm = HiddenMarkovModel(numStates, numFeatures)
    
    # Reload training data.
    reviews = utils.loadLabeledReviews(labeledReviews)
    reviewObjects = []
    for reviewID in reviews.keys():
        review = utils.getReview(reviewID, naiveBayesModel)
        print "(Reload training data with NB model) Review: %s" % reviewID
        reviewObjects.append(review)
        
    # First pass - transition matrix M, probability distribution p, feature means.
    for review in reviewObjects:
        numSentences = len(review.sentences)
        try: numSummarySentences = len(reviews[review.id])
        except TypeError: continue
        
        isSummary = getSummaryFunc(reviews[review.id])
        # Determine initial state and increment p array.
        state = 1 if isSummary(0) else 0
        hmm.incrementP(state)

        for i in range(numSentences):
            # Update feature means.
            hmm.B.updateMu(state, review.sentences[i].phi)
            if i < numSentences - 1:
                # Update transition probability matrix.
                newState = getSucc(state, isSummary(i+1), N)
                hmm.incrementM(state, newState) 
                state = newState

    # Second pass - feature covariance matrices
    for review in reviewObjects:
        numSentences = len(review.sentences)
        try: numSummarySentences = len(reviews[review.id])
        except TypeError: continue
        
        isSummary = getSummaryFunc(reviews[review.id])
        state = 1 if isSummary(0) else 0
        for i in range(numSentences):
            hmm.B.updateSigma(state, review.sentences[i].phi)
            if i < numSentences - 1:
                state = getSucc(state, isSummary(i+1), N)

    # Normalize
    hmm.normalize()
    return hmm 
  
"""
Function: getSucc
Obtain successor state given current state. Next state is dependent on whether next sentence is a summary
sentence and number of summary states.
@param state - current state of the HMM.
@param isSummary - whether the next sentence is a summary sentence.
@param N - number of summary states used in HMM.
"""
def getSucc(state, isSummary, N):

    #special case - simple two state Markov Model
    if N == 1: return 1 if isSummary else 0
    
    # Last summary state
    # Only one transition according to Conroy and O'leary paper, but this should be reconsidered.
    if state == 2*N - 1: return state+1
    
    # Last non-summary state
    if state == 2*N: return state-1 if isSummary else state

    if state % 2: return state + 2 if isSummary else state+1 #intermediate summary state
    else: return state + 1 if isSummary else state           #intermediate non-summary state

"""
Function: norm
Normalize input matrix.
"""
def norm(A):
    numRows, numCols = A.shape
    for i in range(numRows):
        if sum(A[i]) != 0: A[i] = A[i]/sum(A[i])
    return A